Roles
=====

.. _roles:


Inline code highlighting
------------------------

******
:code:
******

.. role:: python(code)
   :language: python

In Python, :python:`1 + 2` is equal to :python:`3`.

Math
----

******
:math:
******

Since Pythagoras, we know that :math:`a^2 + b^2 = c^2`.



Other semantic markup
---------------------

***************
:menuselection:
***************

:menuselection:`Start --> Programs`

