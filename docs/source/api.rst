API
===

.. _lumache:

Lumache
-------

Lumache - Python library for cooks and food lovers

FUNCTIONS
   ``lumache.get_random_ingredients():`` Return a list of random ingredients as strings
