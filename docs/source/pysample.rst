Python Code Sample
==================


Code Example 
------------

.. _Code Example:

Python 3.12 introduced type parameters, which are type variables declared directly within the class or function definition:

.. code-block:: console
   
   class AnimalList[AnimalT](list[AnimalT]):
   		...

	def add[T](a: T, b: T) -> T:
   		return a + b

The corresponding reStructuredText documentation would be:

.. code-block:: console
   
   .. py:class:: AnimalList[AnimalT]

   .. py:function:: add[T](a: T, b: T) -> T

This will render like this:

.. py:class:: AnimalList[AnimalT]
.. py:function:: add[T](a: T, b: T) -> T




Info field lists
----------------

.. _Info field lists:

The field names must consist of one of these keywords and an argument (except for returns and rtype, which do not need an argument). This is best explained by an example:

.. code-block:: console
	
	.. py:function:: send_message(sender, recipient, message_body, [priority=1])

	   Send a message to a recipient

	   :param str sender: The person sending the message
	   :param str recipient: The recipient of the message
	   :param str message_body: The body of the message
	   :param priority: The priority of the message, can be a number 1-5
	   :type priority: integer or None
	   :return: the message id
	   :rtype: int
	   :raises ValueError: if the message_body exceeds 160 characters
	   :raises TypeError: if the message_body is not a basestring

This will render like this:

.. py:function:: send_message(sender, recipient, message_body, [priority=1])

   Send a message to a recipient

   :param str sender: The person sending the message
   :param str recipient: The recipient of the message
   :param str message_body: The body of the message
   :param priority: The priority of the message, can be a number 1-5
   :type priority: integer or None
   :return: the message id
   :rtype: int
   :raises ValueError: if the message_body exceeds 160 characters
   :raises TypeError: if the message_body is not a basestring



List
----

.. _List:

Container types such as lists and dictionaries can be linked automatically using the following syntax:

:type priorities: list(int)
:type priorities: list[int]
:type mapping: dict(str, int)
:type mapping: dict[str, int]
:type point: tuple(float, float)
:type point: tuple[float, float]


Multiple types in a type field will be linked automatically if separated by the word “or”:

:type an_arg: int or None
:vartype a_var: str or int
:rtype: float or str


Style
-----

.. role:: python(code)
  :language: python
  :class: highlight

:python:`print("Hello World")`

