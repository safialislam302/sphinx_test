reStructuredText
================

.. _reStructuredText:

reStructuredText
----------------


#############
Inline markup
#############
one asterisk: *text* for emphasis (italics),

two asterisks: **text** for strong emphasis (boldface), and

backquotes: ``text`` for code samples.

This is a link to the Hyperlink Overview: :ref:`Hyperlinks`

***************************
Lists and Quote-like blocks
***************************
* This is a bulleted list.
* It has two items, the second
  item uses two lines.

1. This is a numbered list.
2. It has two items too.

	a. Test a
	b. Test b

3. Third Item List


#. This is a numbered list.
#. It has two items too.


#. Step 1.
    #. Step a.
    #. Step b.
#. Step 2.

**************
Literal blocks
**************

This is a normal text paragraph. The next paragraph is a code sample::

   It is not processed in any way, except
   that the indentation is removed.

   It can span multiple lines.

This is a normal text paragraph again.


******
Tables
******

=====  =====  =======
A      B      A and B
=====  =====  =======
False  False  False
True   False  False
False  True   False
True   True   True
=====  =====  =======


.. list-table:: Title
   :widths: 25 25 50
   :header-rows: 1

   * - Heading row 1, column 1
     - Heading row 1, column 2
     - Heading row 1, column 3
   * - Row 1, column 1
     -
     - Row 1, column 3
   * - Row 2, column 1
     - Row 2, column 2
     - Row 2, column 3



CSV Files
=========

.. csv-table:: Color RGB
   :file: color_srgb.csv
   :widths: 50, 50, 50
   :header-rows: 1



Hyperlinks
==========

.. _Hyperlinks:

Hyperlinks
----------


#####
Links
#####

See different types of Links.

**************
External links
**************

This is a paragraph that contains `a link`_.

.. _a link: https://domain.invalid/


*********
Citations
*********

Lorem ipsum [Ref]_ dolor sit amet.

.. [Ref] https://nfdixcs.org/


******
Images
******

.. image:: images/NFDI_1.png


*************
Misc Elements
*************

Topic:

.. topic:: My Topic

    My topic text ...

Sidebar:

.. sidebar:: My Sidebar
    
    My sidebar text ...
    
.. code-block:: python

    .. topic:: My Topic
    
        My topic text ...
    
    .. sidebar:: My Sidebar
        
        My sidebar text ...
