Welcome to NFDIxCS documentation!
===================================

**Lumache** (/lu'make/) is a Python library for cooks and food lovers that
creates recipes mixing random ingredients.  It pulls data from the `Open Food
Facts database <https://world.openfoodfacts.org/>`_ and offers a *simple* and
*intuitive* API.

.. comment This is a comment!

Check out the :doc:`usage` section for further information, including how to
:ref:`install <installation>` the project.

.. note::

   This project is under active development.

.. versionadded:: 2.5
   The *spam* parameter.

.. hlist::
   :columns: 3

   * A list of
   * short items
   * that should be
   * displayed
   * horizontally

Showing code examples
---------------------

.. highlight:: c


.. code-block:: ruby

   Some Ruby code.

Enable to generate line numbers for the code block:

.. code-block:: ruby
   :linenos:

   Some more Ruby code.

Set the first line number of the code block. If present, linenos option is also automatically activated:

.. code-block:: ruby
   :lineno-start: 10

   Some more Ruby code, with line numbering starting at 10.


.. warning::
    This is warning text. Use a warning for information the user must
    understand to avoid negative consequences.


Reference
---------

Lorem ipsum [#f1]_ dolor sit amet ... [#f2]_


Contents
--------

.. toctree::
   :name: mastertoc
   :maxdepth: 4
   :numbered:

   pysample
   usage
   api
   role
   reStructuredText
   Glossary
   

.. rubric:: Footnotes

.. [#f1] https://nfdixcs.org/
.. [#f2] https://nfdixcs.org/