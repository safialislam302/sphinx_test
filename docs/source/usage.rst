Usage
=====

Install Sphinx
--------------

.. _Install Sphinx:

First active the environment: 

.. code-block:: console
   
   .venv\Scripts\Activate

Then go to the directory ``docs``

.. code-block:: console
   
   cd docs

Finally create the html file:

.. code-block:: console
   
   make html

And if you want to delete the ``doctress`` folder and run then: 

.. code-block:: console

   rmdir /s /q build\doctrees && make html


To emphasize the `code` within a sentence, you can use **``inline code``**.


.. _installation:

Installation
------------

To use Lumache, first install it using pip:

.. code-block:: console

   (.venv) $ pip install lumache


.. _Creating recipes:

Creating recipes
----------------

To retrieve a list of random ingredients,
you can use the ``lumache.get_random_ingredients()`` function:

.. py:function:: lumache.get_random_ingredients(kind=None)

   Return a list of random ingredients as strings.

   :param kind: Optional "kind" of ingredients.
   :type kind: list[str] or None
   :raise lumache.InvalidKindError: If the kind is invalid.
   :return: The ingredients list.
   :rtype: list[str]


The ``kind`` parameter should be either ``"meat"``, ``"fish"``,
or ``"veggies"``. Otherwise, :py:func:`lumache.get_random_ingredients`
will raise an exception.


.. py:exception:: lumache.InvalidKindError

   Raised if the kind is invalid.